# Building a 2 Tier App and Deploying it via AWS

## Using and Configuring AWS

- Copy the `eng89_devops.pem` file to the `.ssh` folder located in your User folder
- Use the link provided in the .csv file provided by the trainer
- Enter your username and temporary password also provided in the `.csv` file
- Change the old password into a permanent one, making sure it can be remembered and it is secure
- Once logged into AWS, change the data server (found in the top right) to `Europe (Ireland)eu-west-1`
- When that's completed, you're ready to create a VM in AWS!

## Creating the `app` EC2 instance
### Step 1: Choose an Amazon Machine Image (AMI)
- On, AWS, click `Launch instances`
- Select Ubuntu Server 16.04 LTS (HVM), SSD Volume Type
- Ensure the correct architecture is selected (In this instance, 64-bit (x86) is used)

### Step 2: Choose an Instance Type
- For Instance Type, the server isn't resource intensive, so a `t2 micro` machine is sufficient

### Step 3: Configure Instance Details
- Change the Subnet to `DevOpsStudent default 1a | default in eu-west-1a`
- For Auto-assign Public IP, ensure the setting is set to Enable

### Step 4: Add Storage
- For storage, the default size of 8GB is more than sufficient and we can carry on the next step

### Step 5: Add Tags
- Add a new tag and under `Key` type in `name`
- For this, the instance needs to be named, we can use the naming convention `eng89_yourname_app` in the value section so it can be found easily and others can easily identify it

### Step 6: Configure Security Group
- For security group, if one isn't already created, a new one can be created.
- We can follow a similar naming convention to make it easy to find later on `eng89_yourname_app_SG` and a suitable description can be added e.g., "Security Group used for app instance"
- Because the app needs be privately accessed to install packages and setup the nodes, we need to enable private access
- If a rule hasn't been added, a new one can be added by clicking `Add Rule`
- Ensure the `Type` is `SSH` and for `Source`, select `My IP`. This allows the ability to SSH into the VM to install packages
- A suitable description can also be added to the Rule e.g., For private access only
- For this instance, it needs global access too so others can use the app, so a new rule needs to be added
- This time the `Type` should be `HTTP` and the `Source` is set to Anywhere. A suitable description can also be added e.g., "For Global Access"

### Review
- Once everything is configured, the instance can be double-checked to see if everything is set-up correctly
- If everything is set-up correctly, the key pair can be selected and the instance can be launched.
- Select `Choose an existing key pair` and in the `Select a key pair` menu, select `eng89_devops` and tick the box to acknowledge that you have access you have access to the private key

## Creating the `db` EC2 instance
- For creating this instance, almost everything is identical to the `app` instance. Follow steps 1-4 for creating a `db` instance.

### Adding Tags
- For adding tags, it's more of the same, but because it's a different instance, the name needs to be different as well.
- The name of this instance can be `eng89_yourname_db` following the same naming convention.

### Configuring Security Group
- Like the `app` instance, it needs to be privately accessed to update and install packages.
- The `Type` can be set to `SSH` and `Source` set to `My IP`
- The `db` instance doesn't need global access but needs to accessed by the `app` instance so a new rule needs to be added to accommodate for this
- For this Rule, the `Type` is set to `Custom TCP Rule` and under `Port Range`, enter `27017`
- For the `Source` of this Rule, select `Custom` and enter the public IP of the `app` instance and include `/32` at the end
- To find the public IP of the `app` instance, navigate to the instances page on AWS or use [this link](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:)
- Search for the `app` instance created earlier using the naming convention as a guide
- Click on the hyperlinked `Instance ID` and the public IP used here is listed under `Public IPv4 address`
![Finding Public IP](https://gitlab.com/RayWLMo/2_Tier_App_Deployment/-/raw/main/images/Instance_Summary.png)

### Review

- Once everything such as the AMI and the Security Group is checked over and confirmed to be correct, the `db` instance can be launched

## Configuring the `app` instance
- If both instances are running norminally, we can configure each one by installing packages specific for each one

### Copying the `app` folder to the 'app' instance
- For this project, the app folder from the previous vagrant project is needed. It can be found [here](https://gitlab.com/RayWLMo/Virtual_Machines)
- Open Git Bash (in administrator mode) and navigate to the `.ssh` folder using the `cd` command
- Using the `scp` command, we can copy the app folder to the `app` instance
  - `scp -i "eng89_devops.pem" -r "C:\Directory_To_app_folder\app" ubuntu@ec2-YOUR_PUBLIC_IP.eu-west-1.compute.amazonaws.com:/home/ubuntu/app`
  - `-i "eng89_devops.pem"` - provides authentication to copy the files to the instance
  - `-r "C:\Directory_To_app_folder\app"` - provides the directory of what needs to be copied
  - `ubuntu@ec2-YOUR_PUBLIC_IP.eu-west-1.compute.amazonaws.com:/home/ubuntu/app` - provides the directory of the copy destination
  - Note: This will take a while as a lot of individual files are being copied over
  - If the folder name didn't change to `app`, use `mv old_folder_name new_folder_name` to change the directory name

### Entering the `app` instance
- On Git Bash, in the `.ssh` folder, the `eng89_devops.pem` file needs to be made executable. It can be done by using `chmod 400 eng89_devops.pem`
- After this, the instance can be accessed using the `SSH` key - `ssh -i "eng89_devops.pem" ubuntu@ec2-YOUR-PUBLIC_IP.eu-west-1.compute.amazonaws.com`
- Alternatively, these commands can be copied from the AWS website by navigating to the [instances page](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:), selecting the instance you want to connect to and clicking `connect` located towards the top of the site.
- Here, the public IP can be found and under the `SSH client` tab the above instructions and commands can be found
![Connecting to instance](https://gitlab.com/RayWLMo/2_Tier_App_Deployment/-/raw/main/images/Connect_To_Instance_Example.png)

### Installing packages in the `app` instance
- Because the instance is running the same OS as the previous vagrant machines (Ubuntu 16.04), the same commands can be used
```shell
# Updates the package listings
sudo apt-get update -y
# Upgrades any available packages installed
sudo apt-get upgrade -y
# Installing nginx package
sudo apt-get install nginx -y
# updates nodejs package for upgrade
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
# Updates the nodejs into higher version
sudo apt-get install nodejs -y
# Installs python dependencies
sudo apt-get install python-software-properties -y
# Using npm to install pm2 package
sudo npm install -g pm2
# Setting the permanent env variable to connect to MongoDB
sudo echo 'export DB_HOST=mongodb' >> .bashrc
# Running the source file
source ~/.bashrc
# Checking if the variable has been established
printenv DB_HOST
# Navigates the program to the app folder
cd app
# Runs the app.js node
npm start
# Or Alternatively
node app/app.js
```

Just like with Vagrant, if everything runs fine the web server should be running and the console with output a message: `default: Your app is ready and listening on port 3000`

To close the web server or exit the program, use `Ctrl` + `c`

To set up the reverse proxy, the programs needs to be navigated to the `sites-available` folder to access the `default` file
```shell
# Navigates to 'sites-available' folder
cd /etc/nginx/sites-available
# Opens the 'default' file to edit
sudo nano default
# Use Ctrl + K to quickly clear the file, line by line
# Copy and paste the following text to configure the reverse proxy
# Make sure to substitute in the correct public IP
server{
        listen 80;
        server_name _;
        location / {
          proxy_pass http://YOUR_PUBLIC_IP:3000;
          proxy_http_version 1.1;
          proxy_set_header Upgrade \$http_upgrade;
          proxy_set_header Connection 'upgrade';
          proxy_set_header Host \$host;
          proxy_cache_bypass \$http_upgrade;
        }
}
```
- To exit `nano` - `Ctrl` + `x`
- Save by pressing `y` and then `Enter`

To restart nginx
```shell
# Restarts nginx
sudo systemctl restart nginx
sudo systemctl enable nginx
# Check status
systemctl status nginx
```
If everything is running correctly, the web server can be started again using `node app/app.js`

## Configuring the `db` instance
- As the `db` instance only needs to MongoDB package to connect to the `app` instance, there's no need to transfer any files to the VM
- Another Git Bash terminal can be opened so that you can continue to use the `app` instance whilst configuring the `db` instance

### Installing and Configuring MongoDB
- As mentioned earlier, the same commands used with vagrant can be used with this VM/instance

```shell
# Updates the mongodb package so it can be upgraded
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 –recv D68FA50FEA312927
echo "deb https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
# Updates the package list that may need upgrading
sudo apt-get update -y
# Fetch new versions of packages existing on the machine
sudo apt-get upgrade -y
# Installs mongodb package
sudo apt-get install -y mongodb-org=3.2.20 mongodb-org-server=3.2.20 mongodb-org-shell=3.2.20 mongodb-org-mongos=3.2.20 mongodb-org-tools=3.2.20 --allow-unauthenticated
```

Now that MongoDB is installed, the `bind IP` needs to be configured
```shell
# Navigating to the /etc directory
cd /etc
# Opening the 'mongod.conf' file for editing
sudo nano mongod.conf
```
In the `mongod.conf` navigate to the `bindIp` and change it so it's configured to `0.0.0.0`
- It should look like this - `bindIp: 192.168.10.150`

If not already configured, make sure the port number above, is configured to the same one used in the `app` instance, in this case it's `27017`
 - It should look like this - `port: 27017`

- To exit `nano` - `Ctrl` + `x`
- Save by pressing `y` and then `Enter`

Once mongoDB is configured, it can be restarted ensuring that it works
```shell
# Restarting mongodb
sudo systemctl restart mongod
sudo systemctl enable mongod
```
![AWS EC2 Diagram](https://gitlab.com/RayWLMo/2_Tier_App_Deployment/-/raw/main/images/AWS%20EC2%20Diagram.png)
